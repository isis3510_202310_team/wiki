# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.
from dash import Dash, html, dcc
import plotly.express as px
import plotly.graph_objs as go

import pandas as pd
import random

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

from datetime import datetime

from dateutil.parser import parse

import dash_leaflet as dl
import dash_leaflet.express as dlx
from dash_extensions.javascript import assign

cred = credentials.Certificate('sportify-2c499-firebase-adminsdk-ue2q1-d8820390b6.json')
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://sportify-2c499-default-rtdb.firebaseio.com'
})

# Load events from firebase
ref = db.reference("/events/")
events = ref.get()
df = pd.DataFrame.from_dict(events, orient='index')
df_bqs_android = df.copy()
print(df.columns)
df = df.explode('sports')

fig = px.histogram(
    df,
    x='sports',
    title='Most Popular Sport',
    category_orders={'sports': df['sports'].value_counts().index}
)

fig.update_traces(marker_color='#6750A4')

mapUs = px.scatter_geo(
    df,
    lat="latitude",
    lon="longitude",
    scope="usa",
)

mapUs.update_traces(marker_color='#6750A4')

mapCo = px.scatter_geo(
    df,
    lat="latitude",
    lon="longitude",
    center={"lat": 4.6097, "lon": -74.0817},
    scope="south america"
)

mapCo.update_traces(marker_color='#6750A4')

coordinates = []
for index, row in df.iterrows():
    coordinates.append(dict(lat=row["latitude"], lon=row["longitude"]))

# Load the Excel file using Pandas
data = pd.read_excel('timings.xlsx')

# Extract the column of data containing the numbers
numbers = data['Numbers']

# Calculate the average of the numbers
average = numbers.mean()

# Extract the column of data containing the numbers
numbers = data['Numbers']

app = Dash(__name__)

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options

# BQ:

df_bqs_android['date'] = pd.to_datetime(df_bqs_android['date'], format='%d/%m/%Y')
df_bqs_android['day_of_week'] = df_bqs_android['date'].dt.day_name()

event_counts = df_bqs_android['day_of_week'].value_counts().sort_index()

# Create a fixed list of weekdays
weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

# Reindex the event_counts Series with the weekdays list
event_counts = event_counts.reindex(weekdays, fill_value=0)

fig_num_events = go.Figure(data=go.Bar(x=event_counts.index, y=event_counts.values))
fig_num_events.update_traces(marker_color='#6750A4')
fig_num_events.update_layout(
    title='Number of Events by Day of the Week',
    xaxis_title='Day of the Week',
    yaxis_title='Number of Events'
)


def format_start_time(start_time):
    try:
        dt = datetime.strptime(start_time, '%I:%M %p')
        return dt.strftime('%I:%M %p')
    except Exception as e:
        hour, minute_period = start_time.split(':')
        hour = int(hour)
        minute, period = minute_period.strip().split(' ')
        minute = int(minute)
        period = period.strip().upper()

        if period == 'P.M.':
            period = 'PM'
            hour -= 12
        elif period == 'A.M.':
            period = 'AM'

        return f"{hour:02d}:{minute:02d} {period}"


df_bqs_android['startTime'] = df_bqs_android['startTime'].apply(format_start_time)
df_bqs_android['startTime'] = pd.to_datetime(df_bqs_android['startTime'], format='%I:%M %p')
df_bqs_android['hour_range'] = df_bqs_android['startTime'].dt.floor('H').dt.time.astype(str)

hour_counts = df_bqs_android['hour_range'].value_counts().sort_index()

hour_counts.index = hour_counts.index.map(lambda x: f"{x[:5]} - {str(int(x[:2]) + 1)}:{x[3:5]}")

fig_hour_events = go.Figure(data=go.Bar(x=hour_counts.index, y=hour_counts.values))
fig_hour_events.update_traces(marker_color='#6750A4')
fig_hour_events.update_layout(
    title='Number of Events by Hour Range',
    xaxis_title='Number of Events',
    yaxis_title='Hour Range'
)

app.layout = html.Div(children=[
    html.Div([
        html.H2(children='Most Popular Sports',
                style={'text-align': 'center', 'margin-top': '50px', 'font-family': 'Arial', 'font-weight': 'bold',
                       'font-size': '50px'}),
        html.Div([
            html.Div(children='A bar chart showing the most popular sports.',
                     style={'text-align': 'center', 'font-family': 'Arial', 'font-size': '20px'}),

            dcc.Graph(
                id='example-graph',
                figure=fig
            )
        ], className='six columns'),

        html.Div([
            dcc.Graph(id='mapUs', figure=mapUs),
            dcc.Graph(id='mapCo', figure=mapCo),
        ], className='six columns')
    ]),

    dl.Map([
         dl.TileLayer(),
         # From in-memory geojson. All markers at same point forces spiderfy at any zoom level.
         dl.GeoJSON(data=dlx.dicts_to_geojson(coordinates), cluster=True),
         # From hosted asset (best performance).
         dl.GeoJSON(url='assets/leaflet_50k.pbf', format="geobuf", cluster=True, id="sc", zoomToBoundsOnClick=True,
                    superClusterOptions={"radius": 100}),
    ], center=(4.603166, -74.065112), zoom=18, style={'width': '100%', 'height': '50vh', 'margin': "auto", "display": "block"}),

    html.H2(children='Average time to login',
            style={'text-align': 'center', 'margin-top': '50px', 'font-family': 'Arial', 'font-weight': 'bold',
                   'font-size': '50px'}),

    dcc.Graph(id='graph', figure={
        'data': [go.Scatter(x=data.index, y=data['Numbers'], mode='lines')],
        'layout': go.Layout(
            title='Plot the average as a horizontal line',
            shapes=[{
                'type': 'line',
                'xref': 'paper',
                'yref': 'y',
                'x0': 0,
                'x1': 1,
                'y0': data['Numbers'].mean(),
                'y1': data['Numbers'].mean(),
                'line': {
                    'color': 'red',
                    'dash': 'dash'
                }
            }]
        ),

    }),
    html.H2(children='Event Statistics',
            style={'text-align': 'center', 'margin-top': '50px', 'font-family': 'Arial', 'font-weight': 'bold',
                   'font-size': '50px'}),
    html.Div(children='A bar chart showing the number of events made each day of the week.',
             style={'text-align': 'center', 'font-family': 'Arial', 'font-size': '20px'}),
    dcc.Graph(id='event-chart', figure=fig_num_events),
    html.Div(children='A bar chart showing the number of events in each 1-hour range.',
             style={'text-align': 'center', 'font-family': 'Arial', 'font-size': '20px'}),
    dcc.Graph(id='hour-event-chart', figure=fig_hour_events)
])

if __name__ == '__main__':
    app.run_server(debug=True)
